﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;

namespace redolfiweb.Controllers
{
    public class HomeController : Controller
    {   
        private readonly IHostingEnvironment _hostingEnvironment;

        public HomeController(IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
        }

        public IActionResult Index()
        {
            string webRootPath = _hostingEnvironment.WebRootPath;

            string[] files = Directory.GetFiles(webRootPath + "/images/slide");

            for(var i = 0; i < files.Length; i++)
            {
                var fileI = new FileInfo(files[i]).Name;
                files[i] = fileI;
            }

            ViewData["slides"] = files;

            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Historia de pequeña gran empresa";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public ActionResult Ofertas()
        {
            string webRootPath = _hostingEnvironment.WebRootPath;

            IEnumerable<string> files = Directory.EnumerateFiles(webRootPath + "/descargas", "*.pdf");
            var filenames = new List<string>();

            foreach(string file in files)
            {
                var fileI = new FileInfo(file);
                string fileName = fileI.Name;
                filenames.Add(fileName);                
            }

            ViewBag.files = filenames;
            return View();
        }

        public IActionResult Cv()
        {
            return View();
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}
